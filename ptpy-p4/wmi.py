import winreg
import wmi

c = wmi.WMI(computer="192.168.56.101",
            user="administrator",
            password="p@ssw0rd"
            )

EnableVirtualization = c.StdRegProv.GetDWORDValue(hDefKey=winreg.HKEY_LOCAL_MACHINE,
                                                  sSubKeyName="SOFTWARE\Microsoft\Windows\CurrentVersion\policies\system",
                                                  sValueName="EnableVirtualization"
                                                  )

print("EnableVirtualization: " + str(EnableVirtualization[1]))

wql = "select Name, Domain, PartOfDomain from Win32_ComputerSystem"

for ComputerSystem in c.query(wql):
    print("Name: " + ComputerSystem.Name)
    print("Domain: " + ComputerSystem.Domain, ComputerSystem.PartOfDomain)
    print("PartOfDomain: " + str(ComputerSystem.PartOfDomain))